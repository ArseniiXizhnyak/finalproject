﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ReplyModel:BaseModel
    {
        public string Text { get; set; }
        public int ArticleId { get; set; }
        public string Username { get; set; }
    }
}
