﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ArticleModel:BaseModel
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public int BlogId { get; set; }
    }
}
