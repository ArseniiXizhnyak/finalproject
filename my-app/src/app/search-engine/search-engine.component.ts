import { Component, OnInit } from '@angular/core';
import { IArticle } from '../article/article';
import { ArticleService } from '../article/article.service';
import { IBlog } from '../blog/blog';
import { BlogService } from '../blog/blog.service';
import { IEmployee } from '../employee/employee';
import { EmployeeService } from '../employee/employee.service';

@Component({
  selector: 'app-search-engine',
  templateUrl: './search-engine.component.html',
  styleUrls: ['./search-engine.component.css']
})
export class SearchEngineComponent implements OnInit {

  blogs:IBlog[];
  users:IEmployee[];
  articles:IArticle[];

  category: string = "users";
  toSearch: string = "";

  constructor(private usersService: EmployeeService, private blogService: BlogService, private articleService : ArticleService) { }

  ngOnInit(): void {

  }

  radioChangeHandler(event:any){
    this.category = event.target.value;
  }

  search() {
    if(this.category == "users"){
      this.blogs = null;
      this.articles = null;
      this.usersService.getAllByUsername(this.toSearch).subscribe((users) => this.users = users);
    }

    else if(this.category == "blogs"){
      this.users = null;
      this.articles = null;
      this.blogService.getAllByNameSubString(this.toSearch).subscribe((blogs) => this.blogs = blogs);
    }

    else{
      this.blogs = null;
      this.users = null;
      this.articleService.getAllByTitleSubString(this.toSearch).subscribe((articles) => this.articles = articles);
    }
  }
}
