import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms'; 
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { EmployeeService } from './employee/employee.service';
import { UserProfileComponent } from './employee/user.component';
import { BlogComponent } from './blog/blog.component';
import { BlogService } from './blog/blog.service';
import { SearchEngineComponent } from './search-engine/search-engine.component';
import { ArticleComponent } from './article/article.component';
import { ArticleService } from './article/article.service';
import { CommentService } from './comment/comment.service';
import { LoginComponent } from './employee/login/login.component';
import { RegisterComponent } from './employee/register/register.component';
import { MyAccauntComponent } from './employee/my-accaunt/my-accaunt.component';


@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    AppComponent,
    UserProfileComponent,
    BlogComponent,
    SearchEngineComponent,
    ArticleComponent,
    MyAccauntComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    CommonModule
  ],
  providers: [AppComponent,EmployeeService, BlogService, ArticleService, CommentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
