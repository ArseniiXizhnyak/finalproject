import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { IArticle } from '../article/article';
import { ArticleService } from '../article/article.service';
import { IBlog } from './blog';
import { BlogService } from './blog.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit, OnDestroy {
  Id:number = null;
  blog: IBlog;
  articles: IArticle[];
  private sub: Subscription;
  
  constructor(private activatedRoute:ActivatedRoute, private blogService:BlogService, private articleService:ArticleService){}

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe(params => {(this.Id = params['id']); this.blogService.get(this.Id).pipe(map((blog: IBlog) => this.blog = blog)).subscribe()});
    setTimeout(() => {this.getArticles();}, 100);
  }

  getArticles(){
    this.articleService.getAllBlogArticles(this.Id).subscribe((articles) => this.articles = articles);
  }

  ngOnDestroy():void{
    this.sub.unsubscribe();
  }
}
