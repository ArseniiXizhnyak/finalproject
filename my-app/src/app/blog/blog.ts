export interface IBlog {
    Id: number;
    Name: string;
    UserId: number;
}