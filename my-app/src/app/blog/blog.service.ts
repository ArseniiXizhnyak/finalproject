import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IBlog } from './blog';

@Injectable()
export class BlogService {
    
    readonly ROOT_URL = 'https://localhost:44382';

    constructor(private http: HttpClient){}

    getAllUserBlogs(id: number):Observable<IBlog[]>{
        return this.http.get(this.ROOT_URL + '/api/accaunts/'+id+'/blogs').pipe(map((blogs:IBlog[]) => blogs));
    }

    getAllByNameSubString(subString:string): Observable<IBlog[]>{
        return this.http.get(this.ROOT_URL+'/api/blogs/bysubstring/'+subString).pipe(map((blogs:IBlog[]) => blogs));
    }

    get(id: number):Observable<IBlog>{
        return this.http.get(this.ROOT_URL+'/api/blogs/'+id).pipe(map((blog:IBlog) => blog));
    }

    post(blog:IBlog):Observable<IBlog>{
        return this.http.post<any>(this.ROOT_URL + '/api/blogs/create', blog);
    }
}                                                                                    