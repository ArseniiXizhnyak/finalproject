import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { IEmployee } from './employee';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class EmployeeService {

    readonly ROOT_URL = 'https://localhost:44382';

    constructor(private http: HttpClient){}

    getByUsername(username: string): Observable<IEmployee>{
        return this.http.get(this.ROOT_URL+'/api/accaunts/'+username).pipe(map((user:IEmployee) => user));
    }
 
    getAll():Observable<IEmployee[]>{
        return this.http.get(this.ROOT_URL + '/api/accaunts').pipe(map((users:IEmployee[]) => users));
    }

    getAllByUsername(username: string) : Observable<IEmployee[]>{
        return this.http.get(this.ROOT_URL+'/api/accauntsbysubstring/' + username).pipe(map((users:IEmployee[]) => users))
    }

    post(user: IEmployee) :Observable<IEmployee> {
        return this.http.post<IEmployee>(this.ROOT_URL + '/api/register',user);
    }

    login(user: {username:string, password:string}){
        var data = "username="+user.username +"&password="+user.password+"&grant_type=password";
        var reqHeader = new HttpHeaders({'Content-Type':'application/x-www-urlencoded'});
        return this.http.post(this.ROOT_URL+"/token", data,{headers: reqHeader}).pipe(map((user:IEmployee) => 
            {
                return user;
            }
        ));
    }

    getMyPage(): Observable<IEmployee>{
        let header = new HttpHeaders().set(
            "Authorization",
             "bearer " + localStorage.getItem('userToken')
          );
          return this.http.get(this.ROOT_URL + '/api/getuserclaims', {headers:header}).pipe(map((user:IEmployee) => 
            {
                localStorage.setItem('username', user.Username);
                localStorage.setItem('role', user.Role);
                return user;
            }
          ));
    }
}                                                                                  