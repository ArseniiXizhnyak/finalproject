import { NgForOf } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { IEmployee } from '../employee';
import { EmployeeService } from '../employee.service';
import { User } from '../user.class';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  consoleMessage:string;
  haveFailed:boolean = true;
  user:User;
  form:FormGroup;

  constructor(private userService:EmployeeService,private router:Router) { 
    this.user = new User();
  }
  ngOnInit(): void {
    this.haveFailed = false;
  }

  onSubmit(){
    this.userService.post(this.user).subscribe(data => {console.log('Success');this.router.navigate(['/accaunt/login']);}, (error : HttpErrorResponse) => {
      this.handleError(error);
    });
  }

  handleError(error: HttpErrorResponse){
    this.consoleMessage = error.error.Message;
  }
}
