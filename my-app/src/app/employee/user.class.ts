import { IEmployee } from './employee';

export class User implements IEmployee{
    Id: number;
    Username: string;
    Name: string;
    Surname: string;
    Password: string;
    Role:string;
}