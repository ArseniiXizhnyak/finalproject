import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { EmployeeService } from "../employee/employee.service";
import { IEmployee } from './employee';
import { map } from 'rxjs/operators';
import { IBlog } from '../blog/blog';
import { BlogService } from '../blog/blog.service';

@Component({
    selector: 'user',
    templateUrl: './user.component.html',
    providers: [EmployeeService, BlogService]
})

export class UserProfileComponent implements OnInit{

    Username:string = null;
    Blogs: IBlog[];
    user: IEmployee;
    private sub: Subscription;
    isThis:boolean;

    constructor(private activatedRoute:ActivatedRoute, private employeeService: EmployeeService, private blogService:BlogService){}

    ngOnInit():void{
            this.isThis = true;
        this.sub = this.activatedRoute.params.subscribe(params => {(this.Username = params['username']); this.employeeService.getByUsername(this.Username).pipe(map((user: IEmployee) => this.user = user)).subscribe()});
        setTimeout(() => {this.getBlogs();}, 100);
    }

    getBlogs(){
        this.blogService.getAllUserBlogs(this.user.Id).subscribe((blogs) => this.Blogs = blogs,(error) => console.log(1));
    }
}