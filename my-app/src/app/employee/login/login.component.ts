import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLogInError: boolean = false;
  message:string;
  formModel = {
    username:'',
    password:''
  }
  constructor(private userServices: EmployeeService, private router:Router) { }

  ngOnInit(): void {
    this.isLogInError = false;
  }

  onSubmit(){
    this.userServices.login(this.formModel).subscribe((data:any) => {
      localStorage.setItem('userToken', data.access_token);
      this.router.navigate(['/accaunt/my-accaunt']);
    },
    (err:HttpErrorResponse) => { 
      this.isLogInError = true;
    });
  }
}
