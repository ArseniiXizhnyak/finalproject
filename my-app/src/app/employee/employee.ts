export interface IEmployee {
    Id: number;
    Username: string;
    Name: string;
    Surname: string;
    Password: string;
    Role:string;
}