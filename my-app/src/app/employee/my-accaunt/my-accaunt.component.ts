import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { strict } from 'assert';
import { Console } from 'console';
import { error } from 'protractor';
import { IArticle } from 'src/app/article/article';
import { ArticleService } from 'src/app/article/article.service';
import { IBlog } from 'src/app/blog/blog';
import { BlogService } from 'src/app/blog/blog.service';
import { EmployeeService } from '../employee.service';
import { User } from '../user.class';

@Component({
  selector: 'app-my-accaunt',
  templateUrl: './my-accaunt.component.html',
  styleUrls: ['./my-accaunt.component.css']
})
export class MyAccauntComponent implements OnInit {
  
  form={
    Title:'',
    Text:'',
    Id:null,
    BlogId:null
  }
  
  blogError:string;
  currentBlogId:number;
  blogsArticles:IArticle[];
  newName:string;
  blogs:IBlog[];
  isAdmin:boolean = false;
  user:User;

  constructor(private userService:EmployeeService, private blogService:BlogService, private router:Router, private articleService:ArticleService) {  }
  ngOnInit(): void {
    this.blogError = null;
    if(localStorage.getItem('userToken') == null)
      this.router.navigate(['/accaunt/login']);
    else{
      this.userService.getMyPage().subscribe((user) => this.user = user);
      setTimeout(() => {this.getBlogs()}, 200);
    }
  }

  getRole():boolean{
    if(this.user)
      return this.user.Role == "admin";
  }

  getBlogs(){
    this.blogService.getAllUserBlogs(this.user.Id).subscribe((blogs) => this.blogs = blogs);
  }

  createBlog(){
    this.blogService.post({
      Id:null,
      Name:this.newName,
      UserId:this.user.Id
    }).subscribe(data => this.blogError = null, (error) => this.blogError = error.error.Message);
    setTimeout(() => {this.getBlogs();}, 200);
  }

  getBlogsArticles(id:number){
    this.currentBlogId = id;
    this.articleService.getAllBlogArticles(id).subscribe((articles) => this.blogsArticles = articles.reverse(),(error) => console.log(error) );
  }

  onSubmit(){
    this.form.BlogId = this.currentBlogId;
    this.articleService.post(this.form).subscribe((data) => console.log('Success'),(error) => console.log(error));
    setTimeout(() => {this.getBlogsArticles(this.currentBlogId);}, 200);
  }
}
