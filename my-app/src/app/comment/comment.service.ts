import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IComment } from './comment';

@Injectable()
export class CommentService {
    
    readonly ROOT_URL = 'https://localhost:44382';

    constructor(private http: HttpClient){}

    getAllArticleComments(id: number):Observable<IComment[]>{
        return this.http.get(this.ROOT_URL + '/api/articles/'+id+'/replies').pipe(map((blogs:IComment[]) => blogs));
    }

    post(form):Observable<IComment>{
        return this.http.post<IComment>(this.ROOT_URL+'/api/replies/post', form);
    }

    put(id:number,form):Observable<IComment>{
        return this.http.put<IComment>(this.ROOT_URL+'/api/replies/put/'+id, form);
    }

    delete(Id:number):Observable<IComment>{
        return this.http.delete<IComment>(this.ROOT_URL+'/api/replies/'+Id);
    }
}                                                                                    