export interface IComment {
    Id: number;
    BlogId: number;
    UserId:Number;
    Username:string;
}