import {Component, NgModule } from '@angular/core'
import { RoutesRecognized } from '@angular/router'
import { RouterModule, Routes} from '@angular/router'
import { UserProfileComponent } from './employee/user.component';
import { CommonModule } from '@angular/common';
import { BlogComponent } from './blog/blog.component';
import { SearchEngineComponent } from './search-engine/search-engine.component';
import { ArticleComponent } from './article/article.component';
import { LoginComponent } from './employee/login/login.component';
import { RegisterComponent } from './employee/register/register.component';
import { MyAccauntComponent } from './employee/my-accaunt/my-accaunt.component';
const appRoutes:Routes =[
    {path: 'search-engine', component: SearchEngineComponent},
    {path: 'accaunt' , children: [{path: '', component: SearchEngineComponent},{path:'my-accaunt', component: MyAccauntComponent},{path:'login', component: LoginComponent}, {path:'register', component:RegisterComponent}, {path:':username', component: UserProfileComponent} ]},
    {path: 'blog', children:[{path: '', component:SearchEngineComponent}, {path:':id', component: BlogComponent}]},
    {path: 'article', children:[{path: '', component:SearchEngineComponent}, {path:':id', component: ArticleComponent}]}
]

@NgModule({
    imports: [ 
        RouterModule.forRoot(appRoutes),
        CommonModule
    ],
    exports: [RouterModule]
})
export class AppRoutingModule{}