import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
  isLoggedIn():boolean{
    return localStorage.getItem('username') == null;
  }
  logout(){
    localStorage.removeItem('userToken');
    localStorage.removeItem('username');
    localStorage.removeItem('role');
    window.location.reload();
  }
}
