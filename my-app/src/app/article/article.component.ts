import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { IComment } from '../comment/comment';
import { CommentService } from '../comment/comment.service';
import { IArticle } from './article';
import { ArticleService } from './article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  form = {
    Text:'',
    ArticleId:null,
    Username:null
  }

  eform = {
    Text:'',
    ArticleId:null,
    Username:null
  }

  commentUsername:string;
  currentComment:number;
  editedText:string;
  editMode:boolean;
  commentError:string;
  Id:number = null;
  article: IArticle;
  comments: IComment[];
  private sub: Subscription;
  
  constructor(private activatedRoute:ActivatedRoute, private articleService: ArticleService, private commentService:CommentService){}

  ngOnInit(): void {
    this.editMode = false;
    this.sub = this.activatedRoute.params.subscribe(params => {(this.Id = params['id']); this.articleService.get(this.Id).pipe(map((article: IArticle) => this.article= article)).subscribe()});
    setTimeout(() => {this.getComments();}, 200);
  }

  getComments(){
    this.commentService.getAllArticleComments(this.Id).subscribe((comments) => this.comments = comments.reverse());
  }

  onSubmit(){
    this.form.ArticleId = this.article.Id;
    this.form.Username = localStorage.getItem('username');
    this.commentService.post(this.form).subscribe((data) => console.log('Success'), (err:HttpErrorResponse) => console.log(err));
    setTimeout(() => {this.getComments();}, 200);
  }

  isLogged(){
    return localStorage.getItem('username') != null;
  }

  isAdmin(){
    return localStorage.getItem('role') == 'admin';
  }

  edit(Id:number, text:string, username:string){
    this.editedText = text;
    this.currentComment = Id;
    this.editMode = true;
    this.commentUsername = username;
  }

  noEdit(){
    this.currentComment = null;
    this.editMode = false;
  }

  onEditSubmit(){
    this.eform = {Text:this.editedText, ArticleId:this.article.Id, Username:this.commentUsername};
    this.commentService.put(this.currentComment ,this.eform).subscribe((data) => 'Success', (err) => console.log(err));
    setTimeout(() => {this.getComments();}, 200);
  }

  delete(Id:number){
    this.commentService.delete(Id).subscribe((data) => 'Success!',(err) => console.log(err));
    setTimeout(() => {this.getComments();}, 200);
  }
}
