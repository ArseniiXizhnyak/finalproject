import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IArticle } from './article';

@Injectable()
export class ArticleService {
    
    readonly ROOT_URL = 'https://localhost:44382';

    constructor(private http: HttpClient){}

    getAllByTitleSubString(subString:string): Observable<IArticle[]>{
        return this.http.get(this.ROOT_URL+'/api/articles/bysubstring/'+subString).pipe(map((articles:IArticle[]) => articles));
    }

    getAllBlogArticles(id:number):Observable<IArticle[]>{
        return this.http.get(this.ROOT_URL+'/api/blogs/'+id+'/articles').pipe(map((articles:IArticle[]) => articles));
    }

    get(id: number): Observable<IArticle>{
        return this.http.get(this.ROOT_URL+'/api/articles/'+id).pipe(map((article:IArticle) => article));
    }

    post(article:IArticle):Observable<IArticle> {
        return this.http.post<IArticle>(this.ROOT_URL+'/api/articles/create',article);
    }
}                                                                                    