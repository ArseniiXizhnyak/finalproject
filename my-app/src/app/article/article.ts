export interface IArticle {
    Id: number;
    Title: string;
    Text: string;
    BlogId:number;
}