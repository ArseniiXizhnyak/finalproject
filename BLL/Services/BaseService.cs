﻿using BLL.Services.Interface;
using DAL.Repositories.Interface;
using Entities.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public abstract class BaseService : IBaseService
    {
        protected IBaseRepository repository;
        protected BaseService(IBaseRepository repository)
        {
            this.repository = repository;
        }
        public abstract void Add(IBaseEntity entity);

        public abstract void Delete(int id);

        public abstract IBaseEntity Get(int id);

        public abstract void Update(IBaseEntity entity);
    }
}
