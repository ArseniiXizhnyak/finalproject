﻿using DAL.Repositories;
using Domains;
using Entities;
using Entities.Interface;
using Mapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Services
{
    public class BlogService : BaseService
    {
        public BlogService() : base(new BlogRepository()) { }
        public override void Add(IBaseEntity entity)
        {
           if(GetAllUserBlogs((entity as BlogEntity).UserId).ToList().Find(x => x.Name == (entity as BlogEntity).Name) !=null)
                throw new ArgumentException($@"Blog with name {(entity as BlogEntity).Name} already exists");
            repository.Add((entity as BlogEntity).ToDomain());
        }

        public override void Delete(int id)
        {
            repository.Delete(id);
        }

        public override IBaseEntity Get(int id)
        {
            return (repository.Get(id) as BlogDomain).ToEntity();
        }

        public override void Update(IBaseEntity entity)
        {
            repository.Update(entity as BlogDomain);
        }

        public BlogEntity GetByName(string name)
        {
            return (repository as BlogRepository).GetByName(name).ToEntity();
        }

        public IEnumerable<BlogEntity> GetAllUserBlogs(int id)
        {
            return (repository as BlogRepository).GetAllUserBlogs(id).Select(x => x.ToEntity());
        }

        public IEnumerable<BlogEntity> GetAllByNameSubString(string subString)
        {
            return (repository as BlogRepository).GetAllBySubString(subString).Select(x => x.ToEntity());
        }
    }
}
