﻿using Entities.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services.Interface
{
    public interface IBaseService
    {
        IBaseEntity Get(int id);
        void Update(IBaseEntity entity);
        void Delete(int id);
        void Add(IBaseEntity entity);
    }
}
