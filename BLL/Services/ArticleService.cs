﻿using DAL.Repositories.Interface;
using Entities;
using Entities.Interface;
using System;
using Mapper;
using Domains;
using DAL.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Services
{
    public class ArticleService : BaseService
    {
        public ArticleService():base(new ArticleRepository()) { }
        public override void Add(IBaseEntity entity)
        {
            if (string.IsNullOrEmpty((entity as ArticleEntity).Text))
                throw new ArgumentException("Text field is empty");
            if (string.IsNullOrEmpty((entity as ArticleEntity).Title))
                throw new ArgumentException("Title field is empty");
            repository.Add((entity as ArticleEntity).ToDomain());
        }

        public override void Delete(int id)
        {
            repository.Delete(id);
        }

        public override IBaseEntity Get(int id)
        {
            return (repository.Get(id) as ArticleDomain).ToEntity();
        }

        public override void Update(IBaseEntity entity)
        {
            repository.Update((entity as ArticleEntity).ToDomain());
        }

        public IEnumerable<ArticleEntity> GetAllBlogArticles(int id)
        {
            return (repository as ArticleRepository).GetAllBlogArticles(id).Select(x => x.ToEntity());
        }

        public IEnumerable<ArticleEntity> GetAllByTitleSubString(string subString)
        {
            return (repository as ArticleRepository).GetAllByTitleSubString(subString).Select(x => x.ToEntity());
        }
    }
}
