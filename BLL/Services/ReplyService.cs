﻿using DAL.Repositories;
using DAL.Repositories.Interface;
using Domains;
using Entities;
using Entities.Interface;
using Mapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Services
{
    public class ReplyService : BaseService
    {
        public ReplyService() : base(new ReplyRepository()) { }
        public override void Add(IBaseEntity entity)
        {
            repository.Add((entity as ReplyEntity).ToDomain());
        }

        public override void Delete(int id)
        {
            repository.Delete(id);
        }

        public override IBaseEntity Get(int id)
        {
            return (repository.Get(id) as ReplyDomain).ToEntity();
        }

        public override void Update(IBaseEntity entity)
        {
            repository.Update((entity as ReplyEntity).ToDomain());
        }

        public IEnumerable<ReplyEntity> GetAllArticleReplies(int articleId)
        {
            return (repository as ReplyRepository).GetAllArticlesReplies(articleId).Select(x => x.ToEntity());
        }
    }
}
