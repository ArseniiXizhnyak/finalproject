﻿using DAL.Repositories;
using DAL.Repositories.Interface;
using Domains;
using Entities;
using Entities.Interface;
using Mapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Services
{
    public class AccountServices : BaseService
    {
        public AccountServices() : base(new AccountRepository()) { }

        public override void Add(IBaseEntity entity)
        {
            var user = entity as AccauntEntity;
            if (entity == null)
                throw new ArgumentNullException("Argument is null");
            if (user.Name == null || user.Name == "")
                throw new ArgumentException("Name field is empty or null");
            if (user.Surname == null || user.Surname == "")
                throw new ArgumentException("Surname field is empty or null");
            if (user.Password == null || user.Password == "")
                throw new ArgumentException("Password field is empty or null");
            if ((repository as AccountRepository).GetByUsername((entity as AccauntEntity).Username) != null)
                throw new ArgumentException($@"Account with username ""{(entity as AccauntEntity).Username}"" already exists");
            repository.Add((entity as AccauntEntity).ToDomain());
        }

        public override void Delete(int id)
        {
            repository.Delete(id);
        }

        public override IBaseEntity Get(int id)
        {
            return (repository.Get(id) as AccauntDomain).ToEntity();
        }

        public override void Update(IBaseEntity entity)
        {
            repository.Update((entity as AccauntEntity).ToDomain());
        }

        public IEnumerable<AccauntEntity> GetAll()
        {
            return (repository as AccountRepository).GetAll().Select(x => x.ToEntity());
        }

        public AccauntEntity GetByUsername(string username)
        {
            return ((repository as AccountRepository).GetByUsername(username) as AccauntDomain).ToEntity();
        }

        public IEnumerable<AccauntEntity> GetAllBySubString(string username)
        {
            if (username == null)
                throw new ArgumentNullException("Argument is null", nameof(username));
            return (repository as AccountRepository).GetAllBySubString(username).Select(x => x.ToEntity());
        }
    }
}
