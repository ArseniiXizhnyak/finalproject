﻿using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class AccauntSecurity
    {
        public static bool Login(string username, string password)
        {
            AccountRepository accauntRepository = new AccountRepository();

            return accauntRepository.GetAll().Any(accaunt => string.Compare(accaunt.Username, username, StringComparison.InvariantCultureIgnoreCase) == 0 && string.Compare(accaunt.Password, password, StringComparison.InvariantCultureIgnoreCase) == 0);
        }
    }
}
