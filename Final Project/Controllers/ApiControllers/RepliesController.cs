﻿using BLL.Services;
using Entities;
using Mapper;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Final_Project.Controllers.ApiControllers
{
    public class RepliesController : ApiController
    {
        ReplyService replyService;
        public RepliesController()
        {
            replyService = new ReplyService();
        }
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Replies/5
        public ReplyModel Get(int id)
        {
            return (replyService.Get(id) as ReplyEntity).ToModel();
        }

        // POST: api/Replies
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Replies/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Replies/5
        public void Delete(int id)
        {
        }

        public IEnumerable<ReplyModel> GetAllArticlieReplies(int id)
        {
            return replyService.GetAllArticleReplies(id).Select(x => x.ToModel());
        }
    }
}
