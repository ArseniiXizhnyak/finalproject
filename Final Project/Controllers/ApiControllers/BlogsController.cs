﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using BLL.Services;
using Entities;
using Mapper;
using Models;

namespace Final_Project.Controllers.ApiControllers
{
    public class BlogsController : ApiController
    {
        BlogService blogService;
        public BlogsController()
        {
            blogService = new BlogService();
        }
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Blogs/5
        public BlogModel Get(int id)
        {
            return (blogService.Get(id) as BlogEntity).ToModel();
        }

        // POST: api/Blogs
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Blogs/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Blogs/5
        public void Delete(int id)
        {
        }

        public IEnumerable<BlogModel> GetAllUserBlogs(int id)
        {
            return blogService.GetAllUserBlogs(id).Select(x => x.ToModel());
        }

        public BlogModel GetByName(string name)
        {

            return blogService.GetByName(name).ToModel();
        }
    }
}
