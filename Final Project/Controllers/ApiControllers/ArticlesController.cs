﻿using BLL;
using Models;
using System.Collections.Generic;
using System.Web.Http;
using Mapper;
using System.Linq;
using BLL.Services.Interface;
using BLL.Services;
using Entities;
using System.Web.ModelBinding;

namespace Final_Project.Controllers.ApiControllers
{
    public class ArticlesController : ApiController
    {
        ArticleService articleService;

        public ArticlesController()
        {
            articleService = new ArticleService();
        }
        public IEnumerable<UserModel> Get()
        {
            return null;
        }

        // GET api/values/5
        public ArticleModel Get(int id)
        {
            return (articleService.Get(id) as ArticleEntity).ToModel();
        }

        // POST api/values
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            
        }

        public IEnumerable<ArticleModel> GetAllBlogArticles(int blogId)
        {
            return articleService.GetAllBlogArticles(blogId).Select(x => x.ToModel());
        }
    }
}
