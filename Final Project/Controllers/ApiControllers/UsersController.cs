﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using BLL.Services;
using Entities;
using Mapper;
using Models;

namespace Final_Project.Controllers.ApiControllers
{
    public class UsersController : ApiController
    {
        UserService userService;
        public UsersController()
        {
            userService = new UserService();
        }
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/User/5
        [HttpGet]
        public UserModel Get(int id)
        {
            return (userService.Get(id) as UserEntity).ToModel();
        }

        // POST: api/User
        [HttpPost]
        public void Post([FromBody]UserModel value)
        {
            userService.Add(value.ToEntityM());
        }

        // PUT: api/User/5
        public void Put(int id, [FromBody]UserModel value)
        {
            value.Id = id;
            userService.Update(value.ToEntityM());
        }

        // DELETE: api/User/5
        [HttpDelete]
        public void Delete(int id)
        {
            userService.Delete(id);
        }

        public UserModel GetByUsername(string username)
        {
            return (userService.GetByUsername(username) as UserEntity).ToModel();
        }

        public IEnumerable<UserModel> GetAllBySubString(string username)
        {
            return userService.GetAllBySubString(username).Select(x => x.ToModel());
        }
    }
}
