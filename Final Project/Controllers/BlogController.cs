﻿using Final_Project.Controllers.ApiControllers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Final_Project.Controllers
{
    public class BlogController : Controller
    {
        BlogsController blogsController;
        ArticlesController articlesController;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowBlog(int id)
        {
            blogsController = new BlogsController();
            articlesController = new ArticlesController();

            BlogModel blogModel = blogsController.Get(id);
            IEnumerable<ArticleModel> articles = articlesController.GetAllBlogArticles(blogModel.Id);

            return View((blogModel, articles));
        }
    }
}