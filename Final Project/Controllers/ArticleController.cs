﻿using Final_Project.Controllers.ApiControllers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Final_Project.Controllers
{
    public class ArticleController : Controller
    {
        ArticlesController articlesController;
        RepliesController repliesController;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowArticle(int id)
        {
            articlesController = new ArticlesController();
            repliesController = new RepliesController();

            ArticleModel articleModel = articlesController.Get(id);
            IEnumerable<ReplyModel> replies = repliesController.GetAllArticlieReplies(id);

            return View((articleModel,replies));
        }
    }
}