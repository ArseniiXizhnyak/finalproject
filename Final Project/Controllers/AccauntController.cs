﻿using Final_Project.Controllers.ApiControllers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Final_Project.Controllers
{
    public class AccauntController : Controller
    {
        UsersController usersController;
        BlogsController blogsController;
        // GET: Accaunt
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetAccaunt(string username)
        {
            usersController = new UsersController();
            blogsController = new BlogsController();

            UserModel user = usersController.GetByUsername(username);
            IEnumerable<BlogModel> blogModels = blogsController.GetAllUserBlogs(user.Id);

            return View( (user, blogModels) );
        }
         
        public ActionResult FindAllUsers(string username)
        {
            usersController = new UsersController();

            IEnumerable<UserModel> users = usersController.GetAllBySubString(username);

            return View(users);
        }
    }
}