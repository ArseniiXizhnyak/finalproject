﻿using Domains;
using Entities;
using System;

namespace Mapper
{
    public static class ToDomainMapper
    {
        public static AccauntDomain ToDomain(this AccauntEntity user)
        {
            if (user == null)
                return null;
            if (string.IsNullOrWhiteSpace(user.Username))
                throw new ArgumentException(nameof(user.Username), "Users username is empty or null");
            if(string.IsNullOrWhiteSpace(user.Name))
                throw new ArgumentException(nameof(user.Name), "Users name is empty or null");
            if (string.IsNullOrWhiteSpace(user.Surname))
                throw new ArgumentException(nameof(user.Surname), "Users surname is empty or null");
            if (string.IsNullOrWhiteSpace(user.Password))
                throw new ArgumentException(nameof(user.Password), "Users surname is empty or null");
            return new AccauntDomain
            {
                Id = user.Id,
                Username = user.Username,
                Name = user.Name,
                Surname = user.Surname,
                Password = user.Password,
                Role = user.Role
            };
        }

        public static BlogDomain ToDomain(this BlogEntity blog)
        {
            if (blog == null)
                return null;
            if (string.IsNullOrWhiteSpace(blog.Name))
                throw new ArgumentException(nameof(blog), "Blogs name is null or empty");
            return new BlogDomain
            {
                Id = blog.Id,
                UserId = blog.UserId,
                Name = blog.Name
            };
        }

        public static ArticleDomain ToDomain(this ArticleEntity Article)
        {
            if (Article == null)
                return null;
            if (string.IsNullOrWhiteSpace(Article.Text))
                throw new ArgumentException("Article does not contain text",nameof(Article.Text));
            if (string.IsNullOrWhiteSpace(Article.Title))
                throw new ArgumentException("Article does not have title", nameof(Article.Title));
            return new ArticleDomain
            {
                BlogId = Article.BlogId,
                Id = Article.Id,
                Text = Article.Text,
                Title = Article.Title
            };
        }

        public static ReplyDomain ToDomain(this ReplyEntity reply)
        {
            if (reply == null)
                return null;
            if (string.IsNullOrWhiteSpace(reply.Text))
                throw new ArgumentException(nameof(reply.Text), "Reply does not contain text");
            return new ReplyDomain
            {
                ArticleId = reply.ArticleId,
                Id = reply.Id,
                Text = reply.Text,
                Username = reply.Username
            };
        }
    }
}
