﻿using Domains;
using Entities;
using System;

namespace Mapper
{
    public static class ToEntityFromDomainMapper
    {
        public static ArticleEntity ToEntity(this ArticleDomain article)
        {
            if (article == null)
                return null;
            if (string.IsNullOrWhiteSpace(article.Text))
                throw new ArgumentException("Article does not contain text", nameof(article.Text));
            if (string.IsNullOrWhiteSpace(article.Title))
                throw new ArgumentException("Article does not have title", nameof(article.Title));
            return new ArticleEntity
            {
                BlogId = article.BlogId,
                Id = article.Id,
                Text = article.Text,
                Title = article.Title
            };
        }

        public static BlogEntity ToEntity(this BlogDomain blog)
        {
            if (blog == null)
                return null;
            if (string.IsNullOrWhiteSpace(blog.Name))
                throw new ArgumentException(nameof(blog), "Blogs name is null or empty");
            return new BlogEntity
            {
                Id = blog.Id,
                UserId = blog.UserId,
                Name = blog.Name
            };
        }

        public static AccauntEntity ToEntity(this AccauntDomain user)
        {
            if (user == null)
                return null;
            if (string.IsNullOrWhiteSpace(user.Username))
                throw new ArgumentException(nameof(user.Username), "Users username is empty or null");
            if (string.IsNullOrWhiteSpace(user.Name))
                throw new ArgumentException(nameof(user.Name), "Users name is empty or null");
            if (string.IsNullOrWhiteSpace(user.Surname))
                throw new ArgumentException(nameof(user.Surname), "Users surname is empty or null");
            if (string.IsNullOrWhiteSpace(user.Password))
                throw new ArgumentException(nameof(user.Password), "Users surname is empty or null");
            return new AccauntEntity
            {
                Id = user.Id,
                Username = user.Username,
                Surname = user.Surname,
                Name = user.Name,
                Password = user.Password,
                Role = user.Role

            };
        }

        public static ReplyEntity ToEntity(this ReplyDomain reply)
        {
            if (reply == null)
                return null;
            if (string.IsNullOrWhiteSpace(reply.Text))
                throw new ArgumentException(nameof(reply.Text), "Reply does not contain text");
            return new ReplyEntity
            {
                ArticleId = reply.ArticleId,
                Id = reply.Id,
                Text = reply.Text,
                Username = reply.Username
            };
        }
    }
}
