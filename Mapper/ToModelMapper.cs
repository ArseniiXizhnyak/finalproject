﻿using Entities;
using Models;
using System;

namespace Mapper
{
    public static class ToModelMapper
    {
        public static AccauntModel ToModel(this AccauntEntity user)
        {
            if (user == null)
                return null;
            if (string.IsNullOrWhiteSpace(user.Username))
                throw new ArgumentException(nameof(user.Username), "Users username is empty or null");
            if (string.IsNullOrWhiteSpace(user.Name))
                throw new ArgumentException(nameof(user.Name), "Users name is empty or null");
            if (string.IsNullOrWhiteSpace(user.Surname))
                throw new ArgumentException(nameof(user.Surname), "Users surname is empty or null");
            if (string.IsNullOrWhiteSpace(user.Password))
                throw new ArgumentException(nameof(user.Password), "Users surname is empty or null");
            return new AccauntModel
            {
                Id = user.Id,
                Username = user.Username,
                Name = user.Name,
                Surname = user.Surname,
                Password = user.Password,
                Role = user.Role
            };
        }

        public static BlogModel ToModel(this BlogEntity blog)
        {
            if (blog == null)
                return null;
            if (string.IsNullOrWhiteSpace(blog.Name))
                throw new ArgumentException(nameof(blog), "Blogs name is null or empty");
            return new BlogModel
            {
                Id = blog.Id,
                UserId = blog.UserId,
                Name= blog.Name
            };
        }

        public static ReplyModel ToModel(this ReplyEntity reply)
        {
            if (reply == null)
                return null;
            if (string.IsNullOrWhiteSpace(reply.Text))
                throw new ArgumentException(nameof(reply.Text), "Reply does not contain text");
            return new ReplyModel
            {
                Id = reply.Id,
                ArticleId = reply.ArticleId,
                Text = reply.Text,
                Username = reply.Username
            };
        }

        public static ArticleModel ToModel(this ArticleEntity article)
        {
            if (article == null)
                return null;
            if (string.IsNullOrWhiteSpace(article.Text))
                throw new ArgumentException("Article does not contain text", nameof(article.Text));
            if (string.IsNullOrWhiteSpace(article.Title))
                throw new ArgumentException("Article does not have title", nameof(article.Title));
            return new ArticleModel
            {
                BlogId = article.BlogId,
                Id = article.Id,
                Text = article.Text,
                Title = article.Title
            };
        }
    }
}
