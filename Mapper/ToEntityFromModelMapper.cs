﻿using Entities;
using Models;
using System;

namespace Mapper
{
    public static class ToEntityFromModelMapper
    {
        public static ArticleEntity ToEntityM(this ArticleModel article)
        {
            if (article == null)
                return null;
            if (string.IsNullOrWhiteSpace(article.Text))
                throw new ArgumentException("Article does not contain text", nameof(article.Text));
            if (string.IsNullOrWhiteSpace(article.Title))
                throw new ArgumentException("Article does not have title", nameof(article.Title));
            return new ArticleEntity
            {
                BlogId = article.BlogId,
                Id = article.Id,
                Text = article.Text,
                Title = article.Title
            };
        }

        public static BlogEntity ToEntityM(this BlogModel blog)
        {
            if (blog == null)
                return null;
            if (string.IsNullOrWhiteSpace(blog.Name))
                throw new ArgumentException(nameof(blog), "Blogs name is null or empty");
            return new BlogEntity
            {
                Id = blog.Id,
                UserId = blog.UserId,
                Name = blog.Name
            };
        }

        public static AccauntEntity ToEntityM(this AccauntModel user)
        {
            if (user == null)
                return null;
            if (string.IsNullOrWhiteSpace(user.Username))
                throw new ArgumentException("Username field is empty or null");
            if (string.IsNullOrWhiteSpace(user.Name))
                throw new ArgumentException("Name field is empty or null");
            if (string.IsNullOrWhiteSpace(user.Surname))
                throw new ArgumentException("Surname field is empty or null");
            if (string.IsNullOrWhiteSpace(user.Password))
                throw new ArgumentException("Password fied field is empty or null");
            return new AccauntEntity
            {
                Id = user.Id,
                Username = user.Username,
                Surname = user.Surname,
                Name = user.Name,
                Password = user.Password,
                Role = user.Role
            };
        }

        public static ReplyEntity ToEntityM(this ReplyModel reply)
        {
            if (reply == null)
                return null;
            if (string.IsNullOrWhiteSpace(reply.Text))
                throw new ArgumentException(nameof(reply.Text), "Reply does not contain text");
            return new ReplyEntity
            {
                Id = reply.Id,
                ArticleId = reply.ArticleId,
                Text = reply.Text,
                Username = reply.Username
            };
        }
    }
}
