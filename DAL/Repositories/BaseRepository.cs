﻿using DAL.Context;
using DAL.Repositories.Interface;
using Domains.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public abstract class BaseRepository : IBaseRepository
    {
        public abstract void Add(IBaseDomain domain);

        public abstract void Delete(int id);

        public abstract IBaseDomain Get(int id);

        public abstract void Update(IBaseDomain domain);
    }
}
