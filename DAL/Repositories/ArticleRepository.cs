﻿using DAL.Context;
using Domains;
using Domains.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ArticleRepository : BaseRepository
    {
        public ArticleRepository() : base() { }
        public override void Add(IBaseDomain domain)
        {
            using (MyContext context = new MyContext())
            {
                (domain as ArticleDomain).BlogDomain = context.Blogs.Find((domain as ArticleDomain).BlogId);

                context.Articles.Add(domain as ArticleDomain);

                context.SaveChanges();
            }
        }

        public override void Delete(int id)
        {
            using (MyContext context = new MyContext())
            {
                context.Articles.Remove(context.Articles.Find(id));
                context.SaveChanges();
            }
        }

        public override IBaseDomain Get(int id)
        {
            using (MyContext context = new MyContext())
            {
                return context.Articles.Find(id);
            }
        }

        public override void Update(IBaseDomain domain)
        {
            using (MyContext context = new MyContext())
            {
                context.Articles.AddOrUpdate(domain as ArticleDomain);
                context.SaveChanges();
            }
        }

        public IEnumerable<ArticleDomain> GetAllBlogArticles(int id)
        {
            using (MyContext context = new MyContext())
            {
                List<ArticleDomain> result = new List<ArticleDomain>();
                foreach(var article in context.Articles)
                    if (article.BlogId == id)
                        result.Add(article);
                return result;
            }
        }

        public IEnumerable<ArticleDomain> GetAllByTitleSubString(string titleSubString)
        {
            using (MyContext context = new MyContext())
            {
                List<ArticleDomain> articles = new List<ArticleDomain>();
                foreach(var article in context.Articles)
                {
                    if (article.Title.ToLower().Contains(titleSubString.ToLower()))
                        articles.Add(article);
                }
                return articles;
            }
        }
    }
}
