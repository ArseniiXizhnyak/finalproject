﻿using DAL.Context;
using Domains;
using Domains.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class BlogRepository : BaseRepository
    {
        public BlogRepository() : base() { }
        public override void Add(IBaseDomain domain)
        {
            using (MyContext context = new MyContext())
            {
                context.Blogs.Add(domain as BlogDomain);
                (domain as BlogDomain).UserDomain = context.Accaunts.Find((domain as BlogDomain).UserId);
                context.SaveChanges();
            }
        }

        public override void Delete(int id)
        {
            using (MyContext context = new MyContext())
            {
                context.Blogs.Remove(context.Blogs.Find(id));
                context.SaveChanges();
            }
        }

        public override IBaseDomain Get(int id)
        {
            using (MyContext context = new MyContext())
            {
                return context.Blogs.Find(id);
            }
        }

        public override void Update(IBaseDomain domain)
        {
            using (MyContext context = new MyContext())
            {
                context.Blogs.AddOrUpdate(domain as BlogDomain);
                context.SaveChanges();
            }
        }

        public BlogDomain GetByName(string name)
        {
            using (MyContext context = new MyContext())
            {
                return context.Blogs.Where(x => string.Compare(x.Name, name, StringComparison.InvariantCultureIgnoreCase) == 0).FirstOrDefault();
            }
        }

        public IEnumerable<BlogDomain> GetAllUserBlogs(int id)
        {
            using (MyContext context = new MyContext())
            {
                List<BlogDomain> result = new List<BlogDomain>();
                foreach(var x in context.Blogs)
                    if(x.UserId == id)
                        result.Add(x);
                return result;
            }
        }

        public IEnumerable<BlogDomain> GetAllBySubString(string blogNameSubString)
        {
            using (MyContext context = new MyContext())
            {
                List<BlogDomain> blogDomains = new List<BlogDomain>();
                foreach (var blog in context.Blogs)
                    if (blog.Name.ToLower().Contains(blogNameSubString.ToLower()))
                        blogDomains.Add(blog);
                return blogDomains;
            }
        }
    }
}
