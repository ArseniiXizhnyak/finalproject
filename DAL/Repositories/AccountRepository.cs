﻿using DAL.Context;
using Domains;
using Domains.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace DAL.Repositories
{
    public class AccountRepository : BaseRepository
    {
        public override void Add(IBaseDomain domain)
        {
            using (MyContext context = new MyContext())
            {
                context.Accaunts.Add(domain as AccauntDomain);
                context.SaveChanges();
            }
        }

        public override void Delete(int id)
        {
            using (MyContext context = new MyContext())
            {
                context.Accaunts.Remove(context.Accaunts.Find(id));
                context.SaveChanges();
            }
        }

        public override IBaseDomain Get(int id)
        {
            using (MyContext context = new MyContext())
            {
                return context.Accaunts.Find(id);
            }
        }

        public override void Update(IBaseDomain domain)
        {
            using (MyContext context = new MyContext())
            {
                context.Accaunts.AddOrUpdate(domain as AccauntDomain);
                context.SaveChanges();
            }
        }

        public IEnumerable<AccauntDomain> GetAll()
        {
            using (MyContext context = new MyContext())
            {
                List<AccauntDomain> result = new List<AccauntDomain>();
                foreach(var accaunt in context.Accaunts)
                    result.Add(accaunt);
                return result;
            }
        }

        public AccauntDomain GetByUsername(string username)
        {
            using (MyContext context = new MyContext())
            {
                return context.Accaunts.Where(x => string.Compare(x.Username, username, StringComparison.InvariantCultureIgnoreCase) == 0).FirstOrDefault();
            }
        }

        public IEnumerable<AccauntDomain> GetAllBySubString(string username)
        {
            using (MyContext context = new MyContext())
            {
                List<AccauntDomain> userDomains = new List<AccauntDomain>();
                foreach(var user in context.Accaunts)
                    if (user.Username.ToLower().Contains(username.ToLower()))
                        userDomains.Add(user);
                return userDomains;
            }
        }
    }
}
