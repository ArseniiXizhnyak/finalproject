﻿using Domains.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Interface
{
    public interface IBaseRepository
    {
        IBaseDomain Get(int id);
        void Update(IBaseDomain domain);
        void Delete(int id);
        void Add(IBaseDomain domain);
    }
}
