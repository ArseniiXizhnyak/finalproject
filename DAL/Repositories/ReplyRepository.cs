﻿using DAL.Context;
using Domains;
using Domains.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace DAL.Repositories
{
    public class ReplyRepository : BaseRepository
    {
        public override void Add(IBaseDomain domain)
        {
            using (MyContext context = new MyContext())
            {
                (domain as ReplyDomain).ArticleDomain = context.Articles.Find((domain as ReplyDomain).ArticleId);

                context.Replies.Add(domain as ReplyDomain);

                context.SaveChanges();
            }
        }

        public override void Delete(int id)
        {
            using (MyContext context = new MyContext())
            {
                context.Replies.Remove(context.Replies.Find(id));
                context.SaveChanges();
            }
        }

        public override IBaseDomain Get(int id)
        {
            using (MyContext context = new MyContext())
            {
                return context.Replies.Find(id);
            }
        }

        public override void Update(IBaseDomain domain)
        {
            using (MyContext context = new MyContext())
            {
                context.Replies.AddOrUpdate(domain as ReplyDomain);
                context.SaveChanges();
            }
        }

        public IEnumerable<ReplyDomain> GetAllArticlesReplies(int articleId)
        {
            using (MyContext context = new MyContext())
            {
                List<ReplyDomain> result = new List<ReplyDomain>();
                foreach(var reply in context.Replies)
                    if(reply.ArticleId == articleId)
                        result.Add(reply);
                return result;
            }
        }
    }
}
