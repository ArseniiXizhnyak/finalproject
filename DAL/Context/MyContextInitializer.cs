﻿using Domains;
using System.Data.Entity;

namespace DAL.Context
{
    public class MyContextInitializer : DropCreateDatabaseAlways<MyContext>
    {
        protected override void Seed(MyContext context)
        {
            AccauntDomain god = new AccauntDomain { Role = "admin", Username = "admin", Name = "Name1", Surname = "Surname1", Password = "admin" };
            AccauntDomain dorn = new AccauntDomain { Role= "user", Username = "User1", Name = "Name2", Surname = "Surname2", Password = "user" };
            AccauntDomain magnus = new AccauntDomain { Role = "user", Username = "User2", Name = "Name3", Surname = "Surname3", Password = "user" };
            AccauntDomain roboute = new AccauntDomain { Role = "user", Username = "User3", Name = "Name4", Surname = "Surname4", Password = "user" };

            BlogDomain chapter1 = new BlogDomain { Name = "blog1", UserDomain = roboute };
            BlogDomain chapter2 = new BlogDomain { Name = "blog2", UserDomain = roboute };
            BlogDomain chapter3 = new BlogDomain { Name = "blog3", UserDomain = roboute };

            BlogDomain chapter4 = new BlogDomain { Name = "blog4", UserDomain = magnus };
            BlogDomain chapter5 = new BlogDomain { Name = "blog5", UserDomain = magnus };

            BlogDomain chapter6 = new BlogDomain { Name = "blog6", UserDomain = dorn };
            BlogDomain chapter7 = new BlogDomain { Name = "blog7", UserDomain = dorn };
            BlogDomain chapter8 = new BlogDomain { Name = "blog8", UserDomain = dorn };
            BlogDomain chapter9 = new BlogDomain { Name = "blog9", UserDomain = dorn };

            ArticleDomain article1 = new ArticleDomain { BlogDomain = chapter6, Text = "text1", Title = "Title1" };
            ArticleDomain article2 = new ArticleDomain { BlogDomain = chapter6, Text = "text2", Title = "Title2" };
            ArticleDomain article3 = new ArticleDomain { BlogDomain = chapter8, Text = "text3", Title = "Title3" };

            ReplyDomain reply1 = new ReplyDomain {Username =magnus.Username, ArticleDomain = article1, Text = "Reply1" };
            ReplyDomain reply2 = new ReplyDomain {Username =dorn.Username, ArticleDomain = article2, Text = "Reply2" };
            ReplyDomain reply3 = new ReplyDomain {Username = dorn.Username, ArticleDomain = article3, Text = "Reply3" };

            context.Accaunts.Add(god);
            context.Accaunts.Add(dorn);
            context.Accaunts.Add(magnus);
            context.Accaunts.Add(roboute);

            context.Blogs.Add(chapter1);
            context.Blogs.Add(chapter2);
            context.Blogs.Add(chapter3);
            context.Blogs.Add(chapter4);
            context.Blogs.Add(chapter5);
            context.Blogs.Add(chapter6);
            context.Blogs.Add(chapter7);
            context.Blogs.Add(chapter8);
            context.Blogs.Add(chapter9);

            context.Articles.Add(article1);
            context.Articles.Add(article1);
            context.Articles.Add(article1);
            context.Articles.Add(article2);
            context.Articles.Add(article3);

            context.Replies.Add(reply1);
            context.Replies.Add(reply2);
            context.Replies.Add(reply3);

            context.SaveChanges();
        }
    }
}
