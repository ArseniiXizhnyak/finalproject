using Domains;
using System.Data.Entity;
using System.Runtime.CompilerServices;

namespace DAL.Context
{

    public class MyContext : DbContext
    {
        public MyContext() : base("name=MyContext") { 
        
            Database.SetInitializer(new MyContextInitializer());
        }
        public DbSet<BlogDomain> Blogs { get; set; }
        public DbSet<ReplyDomain> Replies { get; set; }
        public DbSet<AccauntDomain> Accaunts { get; set; }
        public DbSet<ArticleDomain> Articles { get; set; }
    }
}