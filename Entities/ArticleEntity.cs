﻿using System;

namespace Entities
{
    public class ArticleEntity: BaseEntity
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public int BlogId { get; set; }
    }
}
