﻿using System;

namespace Entities
{
    public class ReplyEntity:BaseEntity
    {
        public string Text { get; set; }
        public int ArticleId { get; set; }
        public string Username { get; set; }
    }
}
