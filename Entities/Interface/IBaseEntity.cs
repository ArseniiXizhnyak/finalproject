﻿namespace Entities.Interface
{
    public interface IBaseEntity
    {
        int Id { get; set; }
    }
}
