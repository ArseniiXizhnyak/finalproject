﻿using System;

namespace Entities
{
    public class BlogEntity : BaseEntity
    {
        public string Name { get; set; }
        public int UserId { get; set; }
    }
}
