﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using BLL.Services;
using Entities;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Models;
using Owin;

namespace UAI
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            AccountServices accauntServices = new AccountServices();

            var accaunt = accauntServices.GetByUsername(context.UserName);



            if (accaunt != null && accaunt.Password == context.Password)
            {
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                identity.AddClaim(new Claim("Id", accaunt.Id.ToString()));
                identity.AddClaim(new Claim("Username", accaunt.Username));
                identity.AddClaim(new Claim("Name", accaunt.Name));
                identity.AddClaim(new Claim("Surname", accaunt.Surname));
                identity.AddClaim(new Claim("Password", accaunt.Password));
                identity.AddClaim(new Claim(ClaimTypes.Role, accaunt.Role));
                context.Validated(identity);

                return;
            }
            else return;
        }
    }
}
