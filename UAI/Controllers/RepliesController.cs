﻿using BLL.Services;
using Entities;
using Mapper;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace UAI.Controllers
{
    public class RepliesController : ApiController
    {
        ReplyService replyService;
        [Route("api/articles/{id}/replies")]
        public IEnumerable<ReplyModel> GetAllArticleReplies(int id)
        {
            replyService = new ReplyService();

            return (replyService as ReplyService).GetAllArticleReplies(id).Select(x => x.ToModel());
        }

        [Route("api/replies/{id}")]
        public ReplyModel Get(int id)
        {
            replyService = new ReplyService();

            return ((replyService as ReplyService).Get(id) as ReplyEntity).ToModel();
        }

        [Route("api/replies/post")]
        public void Post(ReplyModel replyModel)
        {
            replyService = new ReplyService();

            replyService.Add(replyModel.ToEntityM());
        }

        [Route("api/replies/put/{id}")]
        public void Put(int id, ReplyModel replyModel)
        {
            replyService = new ReplyService();

            replyModel.Id = id;

            replyService.Update(replyModel.ToEntityM());
        }

        [Route("api/replies/{id}")]
        public void Delete(int id)
        {
            replyService = new ReplyService();

            replyService.Delete(id);
        }
    }
}
