﻿using BLL.Services;
using Entities;
using Mapper;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace UAI.Controllers
{
    public class ArticlesController : ApiController
    {
        ArticleService articleService;

        [Route("api/articles/{id}")]
        public ArticleModel Get(int id)
        {
            articleService = new ArticleService();

            return (articleService.Get(id) as ArticleEntity).ToModel();
        }

        [Route("api/blogs/{blogId}/articles")]
        public IEnumerable<ArticleModel> GetAllBlogArticles(int blogId)
        {
            articleService = new ArticleService();

            return articleService.GetAllBlogArticles(blogId).Select(x => x.ToModel());
        }

        [Route("api/articles/bysubstring/{subString}")]
        public IEnumerable<ArticleModel> GetAllByTitleSubString(string subString)
        {
            articleService = new ArticleService();

            return articleService.GetAllByTitleSubString(subString).Select(x => x.ToModel());
        }

        [Route("api/articles/create")]
        public HttpResponseMessage Post(ArticleModel articleModel)
        {
            try
            {
                articleService = new ArticleService();

                articleService.Add(articleModel.ToEntityM());

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch(Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        public void Put(int id, ArticleModel articleModel)
        {
            articleService = new ArticleService();

            articleModel.Id = id;

            articleService.Update(articleModel.ToEntityM());
        }

        public void Delete(int id)
        {
            articleService = new ArticleService();

            articleService.Delete(id);
        }

    }
}
