﻿using BLL.Services;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mapper;
using Entities;
using System.Security.Claims;

namespace UAI.Controllers
{
    public class AccauntsController : ApiController
    {
        AccountServices accauntServices;
        [HttpPost]
        [Route("api/register")]
        public HttpResponseMessage Post([FromBody] AccauntModel value)
        {
            try
            {
                accauntServices = new AccountServices();

                value.Role = "user";

                accauntServices.Add(value.ToEntityM());

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ArgumentException e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [Route("api/accaunts")]
        public List<AccauntModel> GetAllUsers()
        {
            accauntServices = new AccountServices();
             
            return accauntServices.GetAll().Select(x => x.ToModel()).ToList();
        }



        [Route("api/accauntsbysubstring/{subString}")]
        public List<AccauntModel> GetAllByUsername(string subString)
        {
            accauntServices = new AccountServices();

            return accauntServices.GetAllBySubString(subString).Select(x => x.ToModel()).ToList();
        }

        // GET api/values/5

        public AccauntModel Get(int id)
        {
            accauntServices = new AccountServices();

            return (accauntServices.Get(id) as AccauntEntity).ToModel();
        }

        [Route("api/accaunts/{username}")]
        public HttpResponseMessage Get(string username)
        {
            accauntServices = new AccountServices();

            return Request.CreateResponse(HttpStatusCode.OK,(accauntServices.GetByUsername(username) as AccauntEntity).ToModel());
        }

        public void Put(int id, [FromBody] AccauntModel value)
        {
            accauntServices = new AccountServices();

            value.Id = id;

            accauntServices.Update(value.ToEntityM());
        }

        public void Delete(int id)
        {
            accauntServices = new AccountServices();

            accauntServices.Delete(id);
        }


        [HttpGet]
        [Route("api/getuserclaims")]
        [Authorize]
        public AccauntModel GetUserClaims()
        {
            var identityClaims = (ClaimsIdentity)User.Identity;
            List<Claim> claims = identityClaims.Claims.ToList();

            string a = claims.Where(x => x.Type == "Id").FirstOrDefault().Value;

            AccauntModel model = new AccauntModel()
            {
                Id= Convert.ToInt32(claims.Where(x => x.Type == "Id").FirstOrDefault().Value),
                Username = claims.Where(x => x.Type == "Username").FirstOrDefault().Value,
                Password = claims.Where(x => x.Type == "Password").FirstOrDefault().Value,
                Name = claims.Where(x => x.Type == "Name").FirstOrDefault().Value,
                Surname = claims.Where(x => x.Type == "Surname").FirstOrDefault().Value,
                Role = claims.Where(x => x.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").FirstOrDefault().Value,
            };
            return model;
        }
    }
}
