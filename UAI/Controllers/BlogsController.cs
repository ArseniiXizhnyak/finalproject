﻿using BLL.Services;
using Mapper;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Entities;

namespace UAI.Controllers
{
    public class BlogsController : ApiController
    {
        BlogService blogServices;

        [Route("api/accaunts/{userId}/blogs")]
        public HttpResponseMessage GetAllAccountBlogs(int userId)
        {
            blogServices = new BlogService();

            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, blogServices.GetAllUserBlogs(userId).Select(x => x.ToModel()));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [Route("api/blogs/{id}")]
        public BlogModel Get(int id)
        {
            blogServices = new BlogService();

            return (blogServices.Get(id) as BlogEntity).ToModel();
        }

        [Route("api/blogs/bysubstring/{subString}")]
        public IEnumerable<BlogModel> GetByNameSubString(string subString)

        {
            blogServices = new BlogService();

            return blogServices.GetAllByNameSubString(subString).Select(x => x.ToModel());
        }

        [Route("api/blogs/create")]
        public HttpResponseMessage Post(BlogModel blogModel)
        {
            try
            {
                blogServices = new BlogService();

                blogServices.Add(blogModel.ToEntityM());

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch(Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        public void Put(int id, BlogModel blogModel)
        {
            blogServices = new BlogService();

            blogModel.Id = id;

            blogServices.Update(blogModel.ToEntityM());
        }

        public void Delete(int id)
        {
            blogServices = new BlogService();

            blogServices.Delete(id);
        }
    }
}
