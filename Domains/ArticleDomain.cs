﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Domains
{
    [Table("Articles")]
    public class ArticleDomain:BaseDomain
    {
        [Required]
        public string Title { get; set; }
        [Required]
        [MaxLength(5000)]
        public string Text { get; set; }

        [ForeignKey("BlogDomain")]
        [Required]
        public int BlogId { get; set; }
        [Required]
        public BlogDomain BlogDomain { get; set; }
    }
}
