﻿namespace Domains.Interface
{
    public interface IBaseDomain
    {
        int Id { get; set; }
    }
}