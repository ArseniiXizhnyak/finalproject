﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domains
{
    [Table("Replies")]
    public class ReplyDomain:BaseDomain
    {
        [Required]
        [MaxLength(2000)]
        public string Text { get; set; }
        [ForeignKey("ArticleDomain")]
        [Required]
        public int ArticleId { get; set; }
        public ArticleDomain ArticleDomain { get; set; }
        [Required]
        public string Username { get; set; }
    }
}
