﻿using Domains.Interface;
using System.ComponentModel.DataAnnotations;

namespace Domains
{
    public abstract class BaseDomain : IBaseDomain
    {
        [Key]
        public int Id { get; set; }
    }
}
