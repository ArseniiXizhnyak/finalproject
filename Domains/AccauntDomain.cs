﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domains
{
    [Table("Users")]
    public class AccauntDomain:BaseDomain
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        [MaxLength(50)]
        public string Password { get; set; }

        public string Role { get; set; }
    }
}
