﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Domains
{
    [Table("Blogs")]
    public class BlogDomain:BaseDomain
    {
        [ForeignKey("UserDomain")]
        [Required]
        public int UserId { get; set; }
        [Required]
        public AccauntDomain UserDomain { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
